/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package camera;

/**
 *
 * @author Mauricio
 */
public class DigitalCameraSimulation {

    public static void main(String[] args) {
        DigitalCamera dcPSC = new PointAndShootCamera("Canon", "PowerShot A590", 8.0, "16GB");
        DigitalCamera dcPC = new PhoneCamera("Apple", "iPhone", 6.0, "64GB");

        System.out.println(dcPSC.toString());
        System.out.println();
        System.out.println(dcPC.toString());
    }
}
