/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package camera;

/**
 *
 * @author Mauricio
 */
abstract class DigitalCamera {

    String make;
    String model;
    double megapixels;

    public DigitalCamera(String make, String model, double megapixels) {
        this.make = make;
        this.model = model;
        this.megapixels = megapixels;
    }

    public abstract String toString();

    public String describeCamera() {
        return make + "\n" + model + "\n" + "Megapixels = " + String.valueOf(megapixels) + "\n";
    }
}
