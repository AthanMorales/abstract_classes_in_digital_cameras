/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package camera;

/**
 *
 * @author Mauricio
 */
public class PhoneCamera extends DigitalCamera {

    String inMemorySize;

    public PhoneCamera(String make, String model, double megapixels, String inMemorySize) {
        super(make, model, megapixels);
        this.inMemorySize = inMemorySize;
    }

    @Override
    public String toString() {
        return super.describeCamera() + "External memory size = " + inMemorySize;
    }

}
