/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package camera;

/**
 *
 * @author Mauricio
 */
public class PointAndShootCamera extends DigitalCamera{
    
    String exMemorySize;

    public PointAndShootCamera(String make, String model, double megapixels, String exMemorySize) {
        super(make, model, megapixels);
        this.exMemorySize = exMemorySize;
    }

    @Override
    public String toString() {
        return super.describeCamera()+"External memory size = "+ exMemorySize;
    }
}
